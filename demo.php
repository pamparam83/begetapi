<?php

declare(strict_types=1);

use Pamparam83\Begetapi\Beget;
use Pamparam83\Begetapi\Cron\CronDTO;

require './vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createMutable(__DIR__);
$dotenv->load();

$beget = new Beget($_ENV['USER_HOST'],$_ENV['PASS']);


// настраиваем задачу на выполнение каждую минуту
$cronTask = new CronDTO(
    command: '/usr/bin/php ~/site.ru/public_html/test.php',minutes: '*',hours: '*',days: '*',months: '*',weekdays: '*'
);
// добавить задачу
var_dump($beget->cron()->add($cronTask));

// получаем всю информацию аккаунта
var_dump($beget->account()->fullInfo());

// список cron команд
var_dump($beget->cron()->getList());

// список резервных копий
var_dump($beget->backup()->getMysqlBackupList());

// список БД из резервной копии
var_dump($beget->backup()->getMysqlList(1111));
