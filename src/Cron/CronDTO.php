<?php

declare(strict_types=1);

namespace Pamparam83\Begetapi\Cron;

/**
 * Class CronDTO
 *
 * id - идентификатор задания;
 * minutes - минуты могут быть от 0 до 59;
 * hours - часы могут быть от 0 до 23;
 * days - день месяца может быть от 1 до 31;
 * months - месяц может быть от 1 до 12;
 * weekdays - день недели может быть от 0 до 7, где 0 и 7 - воскресенье;
 * command - команда.
 */
final class CronDTO
{
    public function __construct(
        public string $command,
        public string $minutes = '*',
        public string $hours = '*',
        public string $days = '*',
        public string $months = '*',
        public string $weekdays = '*',
        public ?int $id = null,
    ) {
    }
}
