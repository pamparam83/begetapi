<?php

declare(strict_types=1);

namespace Pamparam83\Begetapi\Cron;

use Pamparam83\Begetapi\Beget;

/**
 * Class Cron
 *
 * @link https://beget.com/ru/kb/api/funkczii-upravleniya-cron#getlist
 */
final  class Cron
{
    public function __construct(
        private readonly Beget $beget)
    {
    }

    /**
     *  Метод возвращает список всех задач CronTab.
     */
    public function getList(): array
    {
        return $this->beget->send('cron/getList');
    }


    /**
     * Метод добавит новое задание. После добавления задание будет активно.
     * Можно конфигурировать CronTab для выполнения задач не только в определенное время, но и ежеминутно, ежечасно,
     * ежедневно, еженедельно или ежемесячно, используя комбинацию
     *
     *  *\/5 * * * * - запускать команду каждые пять минут;
     *  *\/3 * * * - запускать каждые три часа;
     *  0 12-16 * * * - запускать команду каждый час с 12 до 16 (в 12, 13, 14, 15 и 16);
     *  0 12,16,18 * * * - запускать команду каждый час в 12, 16 и 18 часов.
     *
     * @param CronDTO $dto
     */
    public function add(CronDTO $dto): array
    {
        return $this->beget->send('cron/add', $dto);
    }

    /**
     * Метод изменит указанное задание.
     */
    public function edit(int $id_task, CronDTO $dto): array
    {
        $dto->id = $id_task;
        return $this->beget->send('cron/edit', $dto);
    }
}
