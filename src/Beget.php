<?php

declare(strict_types=1);

namespace Pamparam83\Begetapi;

use GuzzleHttp\Client;
use Pamparam83\Begetapi\Account\Account;
use Pamparam83\Begetapi\Backup\Backup;
use Pamparam83\Begetapi\Cron\Cron;
use RuntimeException;

/**
 *  Class Beget
 * Подробная информация по работе с АПИ
 *
 * @link https://beget.com/ru/kb/api/obshhij-princzip-raboty-s-api#dopolnitelnye-parametry-zaprosa
 */
final  class Beget
{
    private readonly Client $client;

    public function __construct(private readonly string $login, private readonly string $password)
    {
        $this->client = new Client(['base_uri' => 'https://api.beget.com/api/']);
    }

    /**
     *  Получаем данные по Аккаунту
     */
    public function account(): Account
    {
        return new Account($this);
    }

    /**
     *  Получаем доступ к работе с крон задачами
     */
    public function cron(): Cron
    {
        return new Cron($this);
    }

    /**
     *  Получаем доступ к Бэкапу
     */
    public function backup(): Backup
    {
        return new Backup($this);
    }

    /**
     *  Делаем запрос к АПИ Бегет
     */
    public function send(string $url, array|object $input_data = []): array
    {
        $response = $this->client->request('POST', $url, [
            'form_params' => [
                'login' => $this->login,
                'passwd' => $this->password,
                'output_format' => 'json',
                'input_format' => 'json',
                'input_data' => json_encode($input_data, JSON_THROW_ON_ERROR),
            ],

        ]);
        $body = $response->getBody();
        $contents = json_decode($body->getContents(), true, 512, JSON_THROW_ON_ERROR);

        if (!empty($contents['status']) && $contents['status'] === 'error') {
            throw new RuntimeException(sprintf('Error: %s. \n %s', $contents['error_code'], $contents['error_text']));
        }

        if(!empty($contents['answer']['status']) && $contents['answer']['status'] === 'error'){
          $errors = array_map(fn($item) => sprintf(' %s. %s', $item['error_code'], $item['error_text']),$contents['answer']['errors']);
            throw new RuntimeException(sprintf('Errors: %s. ', implode('\n',$errors)));
        }

        return $contents['answer']['result'] ?: [];
    }
}
