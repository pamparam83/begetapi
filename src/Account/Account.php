<?php

declare(strict_types=1);

namespace Pamparam83\Begetapi\Account;

use Pamparam83\Begetapi\Beget;

/**
 * Все доступные свойства по адресу
 *
 * @link https://beget.com/ru/kb/api/funkczii-upravleniya-akkauntom#getaccountinfo
 *
 * ```json
 * "plan_name": "Great",         // Имя тарифа
 * "user_sites": 7,              // Фактическое кол-во сайтов
 * "plan_site": 25,              // Максимальное кол-во сайтов
 * "user_domains": 4,            // Фактическое кол-во доменов
 * "plan_domain": 2147483647,    // Максимальное кол-во доменов
 * "user_mysqlsize": 153,        // Фактический объем БД MySQL
 * "plan_mysql": 2147483647,     // Максимальное кол-во БД MySQL
 * "user_quota": 1283,           // Размер использованной дисковой квоты
 * "plan_quota": 10000,          // Максимальный размер дисковой квоты
 * "user_ftp": 6,                // Фактическое кол-во FTP-аккаунтов
 * "plan_ftp": 25,               // Максимальное кол-во FTP-аккаунтов
 * "user_mail": 18,              // Фактическое кол-во почтовых ящиков
 * "plan_mail": 2147483647,      // Максимальное кол-во почтовых ящиков
 * "user_bash": "\/bin\/bash",   // Используемая командная оболочка
 * "user_rate_current": "12.66", // Текущая стоимость тарифного плана в сутки
 * "user_is_year_plan": "0",     // Используется ли годовая скидка
 * "user_rate_year": 4620,       // Текущая стоимость тарифа в год
 * "user_rate_month": 385,       // Текущая стоимость тарифа в месяц
 * "user_balance": 1339.57,      // Текущий баланс пользователя
 * "server_apache_version": " Apache\/2.2.23 (Unix)", // Версия Apache
 * "server_mysql_version": "5.1.68",                  // Версия MySQL
 * "server_nginx_version": "nginx\/1.1.0",            // Версия Nginx
 * "server_perl_version": "v5.10.1",                  // Версия Perl
 * "server_php_version": "5.2.17",                    // Версия PHP
 * "server_python_version": "Python 2.6.6",           // Версия Python
 * "server_name": "germes",          // Имя сервера
 * "server_cpu_name": "24 * Intel(R) Xeon(R) CPU X5660  @ 2.80GHz", // Процессора
 * "server_memory": "96747",         // Кол-во оперативной памяти (Мб)
 * "server_memorycurrent": 4944,     // Кол-во используемой оперативной памяти
 * "server_loadaverage": "4.05",     // Текущая нагрузка Load Average
 * "server_uptime": "18"             // Аптайм
 * ```
 *
 */
final class Account
{
    private readonly array $result;

    public function __construct(Beget $beget)
    {
        $this->result = $beget->send('user/getAccountInfo');
    }

    /**
     * Полная информация
     */
    public function fullInfo(): array
    {
        return $this->result;
    }

    /**
     *  Количество дней до блокировки
     */
    public function daysToBlock(): int
    {
        $contents = $this->result;
        return (int)$contents['user_days_to_block'];
    }

    /**
     * Текущая нагрузка
     */
    public function getLoadaverage()
    {
        return $this->result['server_loadaverage'];
    }
    /**
     * Свободное дисковое пространство
     */
    public function freeDiscSpace(): int
    {
        return $this->result['plan_quota'] - $this->result['user_quota'];
    }
}
