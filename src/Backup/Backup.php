<?php

declare(strict_types=1);

namespace Pamparam83\Begetapi\Backup;

use Pamparam83\Begetapi\Beget;

/**
 * Управление бэкапами
 *
 * @link https://beget.com/ru/kb/api/funkczii-upravleniya-bekapami
 */
final class Backup
{
    public function __construct(
        private readonly Beget $beget
    ) {
    }

    /**
     * Метод возвращает доступный список резервных файловых копий.
     */
    public function getFileBackupList(): array
    {
        return $this->beget->send('backup/getFileBackupList');
    }

    /**
     * Метод возвращает доступный список резервных копий баз mysql.
     */
    public function getMysqlBackupList(): array
    {
        return $this->beget->send('backup/getMysqlBackupList');
    }

    /**
     * Метод возвращает список файлов и директорий из резервной копии по заданному пути и идентификатору.
     * Дополнительные параметры
     * backup_id - идентификатор резервной копии backup_id, если не задан - значит листинг идет по текущей копии;
     * path - путь от корня домашней директории (например "/site.ru/public_html").
     *
     * Возвращается массив объектов, каждый объект состоит из следующих элементов:
     * name - имя файла или папки;
     * is_dir - признак файл это (0) или папка (1);
     * mtime - время создания файла в формате "Y-m-d H:i:s";
     * size - размер в байтах.
     *
     * @param int    $backup_id
     * @param string $path
     */
    public function getFileList(int $backup_id, string $path): array
    {
        return $this->beget->send('backup/getFileList');
    }

    /**
     * Метод возвращает список баз данных из резервной копии по заданному идентификатору.
     *
     * @return array - Возвращается список имен баз данных.
     */
    public function getMysqlList(int $backup_id): array
    {
        return $this->beget->send('backup/getMysqlList', ['backup_id' => $backup_id]);
    }

    /**
     * Метод возвращает список и статусы заданий по восстановлению и загрузке.
     * Возвращается массив объектов, каждый объект состоит из следующих элементов:
     *
     * id - идентификатор заявки восстановления / скачивания;
     * operation - действие восстановление (restore), скачивание (download);
     * type - подробное действие и тип данных restore / download и file / mysql;
     * date_create - время создания заявки в формате "Y-m-d H:i:s";
     * target_list - массив элеменов в заявке (файлов или баз данных);
     * status - статус выполнения.
     */
    public function getLog(): array
    {
        return $this->beget->send('backup/getLog');
    }

    /**
     * Метод создает заявку на восстановление данных из резервной копии по заданному пути и резервной копии.
     *
     * Дополнительные параметры
     *
     * backup_id - идентификатор резервной копии backup_id;
     * paths - массив (одно или несколько значений) путей для восстановления от корня домашней директории (например "/site.ru/public_html").
     * Возвращается признак удачного или нет выполнения.
     *
     * @return array
     */
    public function restoreFile(int $backup_id, array $paths)
    {
        return $this->beget->send('backup/restoreFile', ['backup_id' => $backup_id,'paths' => $paths,]);
    }

    /**
     * Метод создает заявку на восстановление БД из резервной копии по заданному имени БД и идентификатору резервной копии.
     *
     * Дополнительные параметры
     *
     * backup_id - идентификатор резервной копии backup_id;
     * bases - массив (одно или несколько значений) имен баз данных MySQL для восстановления.
     *
     * Возвращается признак удачного или нет выполнения.
     *
     * @param int   $backup_id
     * @param array $bases
     * @return array
     */
    public function restoreMysql(int $backup_id, array $bases)
    {
        return $this->beget->send('backup/restoreMysql', ['backup_id' => $backup_id,'bases' => $bases,]);
    }

    /**
     * Метод создает заявку на загрузку и выкладывание данных из резервной копии в корень аккаунта.
     * Дополнительные параметры
     *
     * backup_id - идентификатор резервной копии backup_id (необязательный), если не указан то используется текущая копия;
     * paths - массив (одно или несколько значений) путей для восстановления от корня домашней директории (например "/site.ru/public_html").
     * Возвращается признак удачного или нет выполнения.
     *
     * @param int   $backup_id
     * @param array $paths
     * @return array
     */
    public function downloadFile(int $backup_id, array $paths)
    {
        return $this->beget->send('backup/downloadFile', ['backup_id' => $backup_id, 'paths' => $paths,]);
    }

    /**
     * Метод создает заявку на загрузку и выкладывание данных из резервной копии в корень аккаунта.
     * Дополнительные параметры
     *
     * backup_id - идентификатор резервной копии backup_id (необязательный), если не указан то используется текущая копия;
     * bases - массив (одно или несколько значений) имен баз данных MySQL для восстановления.
     * Возвращается признак удачного или нет выполнения.
     *
     * @param int   $backup_id
     * @param array $bases
     * @return array
     */
    public function downloadMysql(int $backup_id, array $bases)
    {
        return $this->beget->send('backup/downloadMysql', ['backup_id' => $backup_id,'bases' => $bases,]);
    }

}
